from department_app import create_app


if __name__ == "__main__":
    app = create_app()
    # my_api.app_context().push()
    app.run(host="localhost", port=8000, debug=True)

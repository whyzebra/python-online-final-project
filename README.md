## Python Online Final Project:  Flask RESTful API for CRUD operations & Web Application

----
## **Flask web app**
Index page example, which allows to **`add`** / **`edit`** / **`delete`** employees:
<div align="center">
    <img src=screenshots/web_app_employees_page.png>
    <p><i>Index page</i></p>
</div>

---

<div align="center">
    <img src=screenshots/web_app_add_employee.png>
    <p><i>Modal window for adding new employees</i></p>
</div>

---

Departments page allows to **`add`** / **`delete`** departments
<div align="center">
    <img src=screenshots/web_app_departments_page.png>
    <p><i>Departments page</i></p>
</div>
<div align="center">
    <img src=screenshots/web_app_add_department.png>
    <p><i>Modal window for adding new department</i></p>
</div>
---
Department page displays amount of employees, average salary, and allows to **`add`** / **`edit`** / **`delete`** employees from/to current department
<div align="center">
    <img src=screenshots/web_app_single_department.png>
    <p><i>Modal window for adding new department</i></p>
</div>

## **Flask RESTful API:**
**Flask RESTful API: `/rest_api`** provides next functionality:
- `'/api/v1/employee/<int:employee_id>'`
    - **GET** returns employee by id
    - **PUT** modifes employee data
    - **DELETE** deletes employee by id
- **`'/api/v1/employees/'`**
    - **GET** returns list of all employees in the database
    - **POST** adds new employee to the database
- **`'/api/v1/employees/born_on_date/<string:date_of_birth>'`**
    - **GET** returns list of employees born on a specified date
- **`'/api/v1/employees/born_in_period/?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD''`**
    - **GET** returns list of employees born in a specified date period
- **`'/api/v1/department/<int:department_id>'`**, 
**`'/api/v1/department/<int:department_id>/employees'`**
    - **GET** returns department data by id
    - **PUT** modifies department data
    - **DELETE** deletes departent by id
- **`'/api/v1/departments/'`**
    - **GET** returns list of all departments
    - **POST** adds new department to database
- **`'/api/v1/department/<int:department_id>/employees_born_on_date/<string:date_of_birth>'`**
    - **GET** returns list of employees from certain department, born on a specified date
- **`'/api/v1/department/<int:department_id>/employees_born_in_period/?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD'`**
    - **GET** returns list of employees from certain department, born in a specified date period

---

### **Example API responses to all requests:**
- **`/api/v1/employee/<int:employee_id>`** : **GET, PUT, DELETE**
    - **GET** example: 
        ```
        GET /api/v1/employe/1

        RESPONSE: 
        {
            "id": 1,
            "first_name": "Bob",
            "last_name": "Lopez",
            "birthday": "1993-12-27",
            "salary": 1500,
            "department_id": 3,
            "department_name": "Finance"
        }
        ```
    - **PUT** example:
        ```
        PUT /api/v1/employee/1
        {
            "first_name": "Alex",
            "last_name": "Lopez",
            "birthday": "1995-12-12",
            "salary": 3500,
            "department_id": 2
        }

        RESPONSE:
        {
            "id": 1,
            "first_name": "Alex",
            "last_name": "Lopez",
            "birthday": "1995-12-12",
            "salary": 3500,
            "department_id": 3,
            "department_name": "Marketing"
        }
        ```
    - **DELETE** example:
        ```
        DELETE /api/v1/employee/1
        RESPONSE: 
        {
            "id": 1,
            "first_name": "Alex",
            "last_name": "Lopez",
            "birthday": "1993-12-27",
            "salary": 3500,
            "department_id": 2,
            "department_name": "-"
        }

        GET /api/v1/employe/1
        RESPONSE: {"error": "employee with id <1> does not exist"}
        ```


- **`'/api/v1/employees/'`**
    - **GET** > returns list of all employees:
        ```
        GET /api/v1/employees
        RESPONSE:
        [
            {
                "id": 1,
                "first_name": "Bob",
                "last_name": "Lopez",
                "birthday": "1993-12-27",
                "salary": 1500,
                "department_id": 3,
                "department_name": "Finance"
            },
            ....
            {
                "id": 134,
                "first_name": "Anne",
                "last_name": "Wilson",
                "birthday": "1989-10-29",
                "salary": 500,
                "department_id": 4,
                "department_name": "R&D"
            }
        ]
        ```
    - **POST** > adds new employee to database
        ```
        POST /api/v1/employees
        {
            "first_name": "Robert",
            "last_name": "Weide",
            "birthday": "1992-8-15",
            "salary": 2200,
            "department_id": 1,
        }

        RESPONSE:
        {
            "id": 104,
            "first_name": "Robert",
            "last_name": "Weide",
            "birthday": "1992-08-15",
            "salary": 2200,
            "department_id": 1,
            "department_name": "HR"
        }
        ```


- **`'/api/v1/employees/born_on_date/<string:date_of_birth>'`**
    - **GET** returns a list of employees born on specified date
        ```
        GET /api/v1/employees/born_on_date/1989-02-08

        RESPONSE:
        [
            {
                "id": 9,
                "first_name": "Silvia",
                "last_name": "Thomas",
                "birthday": "1989-02-08",
                "salary": 15000,
                "department_id": 1,
                "department_name": "HR"
            }
        ]
        ```

- **`'/api/v1/employees/born_in_period/?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD'`**
    - **GET** list of employees born in period between 2 dates
        ```
        GET /api/v1/employees/born_in_period/?start_date=2000-9-08&end_date=2000-12-31

        RESPONSE:
        [
            {
                "id": 7,
                "first_name": "Jack",
                "last_name": "Taylor",
                "birthday": "2000-11-18",
                "salary": 5000,
                "department_id": 7,
                "department_name": "Production"
            },
            {
                "id": 24,
                "first_name": "Anne",
                "last_name": "Martinez",
                "birthday": "2000-12-02",
                "salary": 1500,
                "department_id": 4,
                "department_name": "R&D"
            },
            {
                "id": 33,
                "first_name": "Tim",
                "last_name": "Anderson",
                "birthday": "2000-09-30",
                "salary": 2500,
                "department_id": 3,
                "department_name": "Finance"
            },
            {
                "id": 71,
                "first_name": "Lucy",
                "last_name": "Lopez",
                "birthday": "2000-09-16",
                "salary": 1500,
                "department_id": 1,
                "department_name": "HR"
            }
        ]
        ```

- **`'/api/v1/department/<int:department_id>'`**,  
**`'/api/v1/department/<int:department_id>/employees'`**: **GET, PUT, DELETE**
    - **GET** department info and list of employees:
        ```
        GET api/v1/department/5/employees
        RESPONSE: 
        {
            "id": 5,
            "name": "Sales",
            "employees_num": 14,
            "avg_salary": 3607.1428571428573,
            "employees": [
                {
                    "id": 10,
                    "first_name": "Lucy",
                    "last_name": "Jones",
                    "birthday": "1983-07-05",
                    "salary": 2500
                },
                ...
                {
                    "id": 94,
                    "first_name": "Lucy",
                    "last_name": "Smith",
                    "birthday": "1996-02-13",
                    "salary": 500
                }
            ]
        }
        ```
    - **PUT** updates department name
        ```
        PUT api/v1/department/1
        payload: {"name": "New Department"}

        RESPONSE:
        {
            "name": "New Department Name"
        }

        GET api/v1/department/1
        RESPONSE:
        {
            "id": 1,
            "name": "New Department",
            "employees_num": 17,
            "avg_salary": 5629.411764705882,
            "employees": [...]
        }
        ```
    - **DELETE**
        ```
        DELETE api/v1/department/7

        RESPONSE:
        {
            "id": 7,
            "name": "Production",
            "employees_num": 15,
            "avg_salary": null,
            "employees": [
                {
                    "id": 7,
                    "first_name": "Jack",
                    "last_name": "Taylor",
                    "birthday": "2000-11-18",
                    "salary": 5000
                },
                ...
                {
                    "id": 100,
                    "first_name": "Charlotte",
                    "last_name": "Williams",
                    "birthday": "1996-09-10",
                    "salary": 1000
                }
            ]
        }

        GET api/v1/department/7
        RESPONSE: {"error": "department with id <7> does not exist}
        ```

- **`'/api/v1/departments/'`**: **GET**, **POST**
    - **GET** returns list of departments
        ```
        GET /api/v1/departments/

        RESPONSE:
        [
            {
                "id": 1,
                "name": "HR",
                "employees_num": 17,
                "avg_salary": 5629.411764705882
            },

            ...

            {
                "id": 5,
                "name": "Sales",
                "employees_num": 14,
                "avg_salary": 3607.1428571428573
            },

            ...

            {
                "id": 9,
                "name": "Kekpartment",
                "employees_num": 1,
                "avg_salary": 999.0
            }
        ]
        ```
    - **POST** adds new department to database
        ```
        POST /api/v1/departments/
        payload: {"name": "New Department Name"}

        RESPONSE:
        {
            "name": "New Department Name"
        }

        GET api/v1/department/10
        RESPONSE:
        {
            "id": 10,
            "name": "New Department Name",
            "employees_num": 0,
            "avg_salary": null,
            "employees": []
        }
        ```

- `'/api/v1/department/<int:department_id>/employees_born_on_date/<string:date_of_birth>'`
    - **GET** returns list of employees from specified department, born on specific date
        ```
        GET api/v1/department/2/employees_born_on_date/1983-05-16
        RESPONSE:
        [
            {
                "id": 13,
                "first_name": "Bob",
                "last_name": "Garcia",
                "birthday": "1983-05-16",
                "salary": 500,
                "department_id": 2,
                "department_name": "Marketing"
            }
        ]
        ```

- **`'/api/v1/department/<int:department_id>/employees_born_in_period/'`**
    - **GET** returns list of employees from specified department, born in period between 2 dates
        ```
        GET api/v1/department/3/employees_born_in_period/?start_date=1990-01-01&end_date=1998-12-31

        RESPONSE:
        [
            {
                "id": 5,
                "first_name": "Andrew",
                "last_name": "Wilson",
                "birthday": "1991-06-18",
                "salary": 2500,
                "department_id": 3,
                "department_name": "Finance"
            },
            {
                "id": 44,
                "first_name": "Lucy",
                "last_name": "Davis",
                "birthday": "1997-08-13",
                "salary": 10000,
                "department_id": 3,
                "department_name": "Finance"
            },
            {
                "id": 82,
                "first_name": "Emilia",
                "last_name": "Davis",
                "birthday": "1990-05-01",
                "salary": 15000,
                "department_id": 3,
                "department_name": "Finance"
            }
        ]
        ```

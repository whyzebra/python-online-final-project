from rest_api import create_api


if __name__ == "__main__":
    my_api = create_api()
    my_api.app_context().push()
    my_api.run(debug=True)

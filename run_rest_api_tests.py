from rest_api.tests.test_common import TestCommon
import unittest   


def load_test(test_class):
    return unittest.TestLoader().loadTestsFromTestCase(test_class)


if __name__ == '__main__':
    suite  = unittest.TestSuite()

    suite.addTests(
        [
            load_test(TestCommon), 
        ]
    )

    runner = unittest.TextTestRunner()
    runner.run(suite)

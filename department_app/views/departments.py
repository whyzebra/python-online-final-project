from flask import Flask, render_template, flash, redirect, request, url_for, Blueprint
from department_app.common import (
    add_employee, 
    update_employee, 
    delete_employee, 
    add_department, 
    delete_department
)
import requests


departments = Blueprint('departments', __name__)

@departments.route('/departments', methods=['GET', 'POST'])
def departments_list():
    if request.method == 'GET':
        response = None
        try:    
            response = requests.get("http://localhost:5000/api/v1/departments/")
        except requests.exceptions.ConnectionError:
            return render_template("departments_list.html", title="No data", response=response)

        return render_template("departments_list.html", title="Departments List", response=response.json())

    if request.method == 'POST':
        if 'delete_department' in request.form:
            delete_department()
        
        elif 'add_department' in request.form:
            add_department()
        
        return redirect(url_for('departments.departments_list'))


@departments.route('/department/<int:department_id>', methods=['GET', 'POST'])
def department_page(department_id):
    if request.method == 'POST':
        if 'delete_employee' in request.form:
            delete_employee()

        elif 'update_employee' in request.form:
            update_employee()

        elif 'add_employee' in request.form:
            add_employee()
            
        return redirect(url_for('departments.department_page', department_id=department_id))

    if request.method == 'GET':
        response = None
        try:    
            response = requests.get(f"http://localhost:5000/api/v1/department/{department_id}")
        except requests.exceptions.ConnectionError:
            return render_template("department.html", title="No data")

        if 'error' in response.json():
            return render_template("department.html", title="No data", response=None, error=response.json()['error'])

        return render_template("department.html", title="Departments List", department=response.json())
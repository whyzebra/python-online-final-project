from flask import Flask, render_template, flash, redirect, request, url_for, Blueprint
from department_app.common import (
    add_employee, 
    update_employee, 
    delete_employee
)
import requests

employees = Blueprint('employees', __name__)

@employees.route('/')
@employees.route('/employees', methods=['GET', 'POST'])
def employees_list():
    if request.method == 'GET':
        response = None

        try:    
            response = requests.get("http://localhost:5000/api/v1/employees/")
        except requests.exceptions.ConnectionError:
            return render_template("employees.html", title="No data")

        return render_template("employees.html", title="Employees List", response=response.json())
    
    if request.method == 'POST':
        if 'delete_employee' in request.form:
            delete_employee()

        elif 'update_employee' in request.form:
            update_employee()

        elif 'add_employee' in request.form:
            add_employee()

        return redirect(url_for('employees.employees_list'))

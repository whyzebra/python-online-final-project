from flask import flash, request
import requests
import json


def flash_response_errors(response):
    if response is not None:
        try:
            errors = json.loads(response.text)

            for field in errors:
                flash(f"Warning: {field}: {errors[field]}", 'error')
        except:
            pass


def add_employee():
    try:
        employee_data = {
            'first_name': request.form['first_name'],
            'last_name': request.form['last_name'],
            'salary': int(request.form['salary']),
            'birthday': request.form['birthday'],
            'department_id': request.form['department_id']
        }
    except Exception as e:
        flash('Error during form fields validation', 'error')
        return False

    try:
        response = requests.post(
            f"http://localhost:5000/api/v1/employees/", 
            json=employee_data
        )
        
        if response.status_code == 200:
            flash('New Employee has been successfully added', 'success')

        else:
            flash('An error occured, Employee hasn\'t been added :(', 'error')  
            flash_response_errors(response)

    except requests.exceptions.ConnectionError:
        flash('Api Connection Error, Employee hasn\'t been added :(', 'error')
    except Exception:
        flash('An error occured, Employee hasn\'t been added :(', 'error')  


def update_employee():
    try:
        employee_data = {
            'first_name': request.form['first_name'],
            'last_name': request.form['last_name'],
            'salary': int(request.form['salary']),
            'birthday': request.form['birthday'],
            'department_id': request.form['department_id']
        }
    except Exception as e:
        flash('Error during form fields validation', 'error')
        return False

    try:
        response = requests.put(
            f"http://localhost:5000/api/v1/employee/{request.form['employee_id']}", 
            json=employee_data
        )

        if response.status_code == 200:
            flash('Employee data has been successfully updated', 'success')

        else:
            flash('An error occured, Employee data hasn\'t been updated :(', 'error')
            flash_response_errors(response)

    except requests.exceptions.ConnectionError:
        flash('Api Connection Error, Employee data hasn\'t been updated :(', 'error')


def delete_employee():
    response = None
    
    try:
        response = requests.delete(f"http://localhost:5000/api/v1/employee/{request.form['employee_id']}")

        if response.status_code == 200:
            flash('Employee has been deleted!', 'success')
        else:
            flash('An error occured, Employee hasn\'t been deleted :(', 'error')
            flash_response_errors(response)

    except requests.exceptions.ConnectionError:
        flash('Api Connection Error, Employee hasn\'t been deleted :(', 'error')


def delete_department():
    response = None
    
    try:
        response = requests.delete(f"http://localhost:5000/api/v1/department/{request.form['department_id']}")

        if response.status_code == 200:
            flash('Department has been deleted!', 'success')
        else:
            flash('An error occured, Department hasn\'t been deleted :(', 'error')
            flash_response_errors(response)

    except requests.exceptions.ConnectionError:
        flash('Api Connection Error, Department hasn\'t been deleted :(', 'error')


def add_department():
    response = None
    try:
        department_data = {'name': request.form['department_name']}
    except Exception as e:
        flash('Error during form fields validation', 'error')
        return False

    try:
        response = requests.post(
            f"http://localhost:5000/api/v1/departments/",
            json=department_data
        )

        if response.status_code == 200:
            flash('Department has been added!', 'success')
        else:
            flash('An error occured, Department hasn\'t been added :(', 'error')
            flash_response_errors(response)

    except requests.exceptions.ConnectionError:
        flash('Api Connection Error, Department hasn\'t been added :(', 'error')
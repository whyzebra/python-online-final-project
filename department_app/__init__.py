from flask import Flask


def create_app():
    app = Flask(__name__, template_folder="templates")
    app.secret_key = 'SOME SECRET' 

    from department_app.views.employees import employees
    from department_app.views.departments import departments
    app.register_blueprint(employees)
    app.register_blueprint(departments)
    
    return app

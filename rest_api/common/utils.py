from datetime import datetime


def validate_datetime_format(date):
    try:
        datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
        return False

    return True

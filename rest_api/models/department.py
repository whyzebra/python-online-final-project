from rest_api.models import db


class DepartmentModel(db.Model):
    # __tablename__ = 'department'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    employees = db.relationship('EmployeeModel', backref='department')

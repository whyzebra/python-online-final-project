from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
from .employee import EmployeeModel
from .department import DepartmentModel

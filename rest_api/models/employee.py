from rest_api.models import db


class EmployeeModel(db.Model):
    # __tablename__ = 'employee'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20))
    last_name = db.Column(db.String(20))
    birthday = db.Column(db.DateTime)
    salary = db.Column(db.Integer, default=300)
    department_id = db.Column(db.Integer, db.ForeignKey('department_model.id'), nullable=True)

    def __repr__(self):
        return f'<Employee {self.first_name}: {self.department.name}>'

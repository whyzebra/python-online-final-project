from rest_api.marshmallow_schemas import ma
from rest_api.models import EmployeeModel
from marshmallow.validate import Length, Range
from marshmallow import ValidationError
from rest_api.models import db, DepartmentModel

class EmployeeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = EmployeeModel
        fields = ('id', 'first_name', 'last_name', 'birthday', 'salary', 'department_id', 'department_name')
        ordered = True
    
    def department_id_validation(department_id):
        departments_ids = [dep_id for dep_id, in db.session.query(DepartmentModel.id).all()]

        if department_id not in departments_ids:
            raise ValidationError(f'Department with id={department_id} does not exist')

    first_name = ma.Str(required=True, validate=Length(min=2, max=20))
    last_name = ma.Str(required=True, validate=Length(min=2, max=20))
    birthday = ma.Date('%Y-%m-%d', required=True)
    salary = ma.Integer(required=True, validate=Range(min=400, max=15000))
    department_id = ma.Integer(allow_none=True, validate=department_id_validation)
    department_name = ma.Method("get_department_name")

    def get_department_name(self, obj):
        try:
            return obj.department.name
        # catching sqlalchemy.orm.exc.DetachedInstanceError:
        except:
            return '-'

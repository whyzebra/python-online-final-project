from flask_marshmallow import Marshmallow
ma = Marshmallow()


from .department_schemas import DepartmentSchema, DepartmentsListSchema, DepartmentPureSchema
department_schema = DepartmentSchema()
departments_list_schema = DepartmentsListSchema(many=True)
department_pure_schema = DepartmentPureSchema()


from .employee_schemas import EmployeeSchema
employee_schema = EmployeeSchema()
employees_list_schema = EmployeeSchema(many=True)

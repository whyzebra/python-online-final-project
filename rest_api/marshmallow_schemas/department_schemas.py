from rest_api.models import db, DepartmentModel, EmployeeModel
from rest_api.marshmallow_schemas import ma
from .employee_schemas import EmployeeSchema
from marshmallow.validate import Length


class DepartmentSchema(ma.SQLAlchemySchema):
    class Meta:
        model = DepartmentModel
        fields = ('id', 'name', 'employees_num', 'avg_salary', 'employees')
        ordered = True

    employees_num = ma.Method("count_employees")
    avg_salary = ma.Method("get_avg_salary")
    employees = ma.Nested(EmployeeSchema(only=('id', 'first_name', 'last_name', 'birthday', 'salary')), many=True)

    def count_employees(self, obj):
        return len(obj.employees)

    def get_avg_salary(self, obj):
        return db.session \
                .query(db.func.avg(EmployeeModel.salary))\
                .filter_by(department_id=obj.id)\
                .scalar()


class DepartmentPureSchema(ma.SQLAlchemySchema):
    class Meta:
        model = DepartmentModel
        fields = ('name', )

    name = ma.Str(required=True, validate=Length(min=2, max=40))
    
    
class DepartmentsListSchema(DepartmentSchema):
    class Meta:
        fields = ('id', 'name', 'employees_num', 'avg_salary')

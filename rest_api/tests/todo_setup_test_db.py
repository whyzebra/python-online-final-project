from rest_api import create_app
from rest_api.models import db, DepartmentModel, EmployeeModel
from datetime import date, datetime, timedelta
# from random import choice, shuffle, randint, randrange

NAMES = [
    'Alex', 'Jack', 'Bob', 'Sam', 'Tim', 'Antony', 'Andrew', 
    'Lucy', 'Anne', 'Emma', 'Charlotte', 'Emilia', 'Silvia', 'Yui'
]
SURNAMES = [
    'Smith', 'Johnson', 'Williams','Brown','Jones','Garcia', 'Miller','Davis', 'Rodriguez', 'Martinez',
    'Hernandez','Lopez','Gonzalez','Wilson','Anderson','Thomas','Taylor','Moore','Jackson','Martin'
]
SALARIES = [500, 1000, 1500, 2000, 2500, 5000, 10000, 15000]
DEPARTMENTS = ['Production', 'R&D', 'HR', 'Marketing', 'Sales', 'Accounting', 'Finance']
DB_DEPARTMENTS = []


def shuffle_data(*args):
    for arg in args:
        shuffle(arg)


def random_date():
    start = date(year=1980, month=1, day=1)
    end = date(year=2000, month=12, day=31)

    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)

    return start + timedelta(seconds=random_second)


def refresh_db():
    db.drop_all()
    db.create_all()


def generate_departments():
    for name in DEPARTMENTS:
        department = DepartmentModel(name=name)
        db.session.add(department)
        DB_DEPARTMENTS.append(department)

    db.session.commit()


def generate_employees():
    for _ in range(100):
        employee = EmployeeModel(
            first_name=choice(NAMES),
            last_name=choice(SURNAMES),
            birthday=random_date(),
            salary=choice(SALARIES),
            department=choice(DB_DEPARTMENTS),
        )
        db.session.add(employee)

    db.session.commit()


if __name__ == "__main__":
    app = create_app()
    app.app_context().push()

    refresh_db()
    shuffle_data(NAMES, SURNAMES, SALARIES, DEPARTMENTS)
    generate_departments()
    generate_employees()

from rest_api.common.utils import validate_datetime_format
import unittest


class TestCommon(unittest.TestCase):
    def test_validate_datetime_format(self):
        date_str = '1998-12-26'
        self.assertEqual(validate_datetime_format(date_str), True, "'1998-12-26' is correct date")
        
        date_str = '26-12-1998'
        self.assertEqual(validate_datetime_format(date_str), False, "'26-12-1998': string has wrong date format")

        date_str = 'some string'
        self.assertEqual(validate_datetime_format(date_str), False, "String does not contain date")

        date_str = '1998-13-26'
        self.assertEqual(validate_datetime_format(date_str), False, "'1998-13-26': month '13' does not exist")

        date_str = '1998-12-32'
        self.assertEqual(validate_datetime_format(date_str), False, "'1998-12-32': day '32' does not exist")

from project.service.department_queries import (
    get_department_by_id,
    get_all_departments,
    add_department,
    update_department,
    delete_department
)

import unittest


class TestDepartmentQueries(unittest.TestCase):
    def test_get_department_by_id(self):
        pass
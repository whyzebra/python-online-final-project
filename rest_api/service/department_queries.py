from rest_api.models import DepartmentModel
from rest_api.models import db


def get_department_by_id(department_id):
    return DepartmentModel.query.filter_by(id=department_id).first()


def get_all_departments():
    return DepartmentModel.query.all()


def add_department(data):
    new_department = DepartmentModel(name=data["name"])
    db.session.add(new_department)
    db.session.commit()

    return new_department


def update_department(department, data):
    department.name = data["name"]
    db.session.commit()


def delete_department(department):
    db.session.delete(department)
    db.session.commit()
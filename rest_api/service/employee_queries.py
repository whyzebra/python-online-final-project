from rest_api.models import EmployeeModel
from rest_api.models import DepartmentModel
from rest_api.models import db
from datetime import datetime


def get_employee_by_id(employee_id):
    return EmployeeModel.query.filter_by(id=employee_id).first()


def get_all_employees():
    return EmployeeModel.query.all()


def get_employees_born_on_date(birthday):
    return EmployeeModel.query.filter(EmployeeModel.birthday == birthday).all()

    
def get_employees_born_in_period(start_date, end_date):
    return EmployeeModel.query.filter(
        (EmployeeModel.birthday >= start_date) &
        (EmployeeModel.birthday <= end_date)
    ).all()


def get_employees_born_on_date_from_department(birthday, department_id):
    return EmployeeModel.query.filter(
        (EmployeeModel.birthday == birthday) &
        (EmployeeModel.department_id == department_id)
    ).all()


def get_employees_born_in_period_from_department(start_date, end_date, department_id):
    return EmployeeModel.query.filter(
        (EmployeeModel.birthday >= start_date) &
        (EmployeeModel.birthday <= end_date) &
        (EmployeeModel.department_id == department_id)
    ).all()


def add_employe(data):
    new_employee = EmployeeModel(
        first_name=data["first_name"],
        last_name=data["last_name"],
        salary=data["salary"],
        birthday=datetime.strptime(data["birthday"], '%Y-%m-%d'),
        department_id=data["department_id"]
    )
    db.session.add(new_employee)
    db.session.commit()

    return new_employee


def update_employee(employee, data):
    for field in data:
        if field == 'birthday':
            data[field] = datetime.strptime(data[field], '%Y-%m-%d')

        setattr(employee, field, data[field])

    db.session.commit()


def delete_employee(employee):
    db.session.delete(employee)
    db.session.commit()

from flask import request
from flask_restful import Resource
from rest_api.models import DepartmentModel
from rest_api.common import validate_datetime_format
from datetime import datetime

from rest_api.marshmallow_schemas import (
    department_schema, 
    departments_list_schema,
    department_pure_schema,
    employees_list_schema
)

from rest_api.service import (
    get_department_by_id, 
    get_all_departments, 
    get_employees_born_on_date_from_department,
    get_employees_born_in_period_from_department,

    add_department,
    update_department,
    delete_department
)


class DepartmentResource(Resource):
    def get(self, department_id):
        department = get_department_by_id(department_id)

        if not department:
            return {'error': f'department with id <{department_id}> does not exist'}

        return department_schema.dump(department)
    
    def put(self, department_id):
        department = get_department_by_id(department_id)

        if not department:
            return {'error': f'Department with id <{department_id}> does not exist'}

        data = request.get_json()
        errors = department_pure_schema.validate(data)
    
        if errors:
            return errors, 500

        update_department(department, data)
        return department_pure_schema.dump(department)


    def delete(self, department_id):
        department_to_delete = get_department_by_id(department_id)

        if not department_to_delete:
            return {'error': f'Department with id <{employee_id}> does not exist'}

        delete_department(department_to_delete)
        return department_schema.dump(department_to_delete)
    

class DepartmentsListResource(Resource):
    def get(self):
        all_departments = get_all_departments()

        return departments_list_schema.dump(all_departments)

    def post(self):
        data = request.get_json()
        errors = department_pure_schema.validate(data)
    
        if errors:
            return errors, 500

        new_department = add_department(data)
        return department_pure_schema.dump(new_department)


class DepartmentEmployeesBornOnDateResource(Resource):
    def get(self, department_id, date_of_birth):
        department = get_department_by_id(department_id)

        if not department:
            return {'error': f'department with id <{department_id}> does not exist'}
        
        if not validate_datetime_format(date_of_birth):
            return {"error": f"Incorrect date format: <{date_of_birth}>, use <YYYY-MM-DD> format"}

        birthday = datetime.strptime(date_of_birth, '%Y-%m-%d')
        quqery_result = get_employees_born_on_date_from_department(birthday, department_id)

        if not quqery_result:
            return {"msg": f"There are no employees from {department.name} department, born on {date_of_birth}"}

        return employees_list_schema.dump(quqery_result)


class DepartmentEmployeesBornInPeriodResource(Resource):
    def get(self, department_id):
        department = get_department_by_id(department_id)

        if not department:
            return {'error': f'department with id <{department_id}> does not exist'}

        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')

        if not all([start_date, end_date]):
            return {"error": f"use format <?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD> to get all Employees from {department.name} department, born in a specific period"}
   
        if not validate_datetime_format(start_date):
            return {"error": f"Incorrect date format for 'start_date': <{start_date}>, use <YYYY-MM-DD> format"}

        if not validate_datetime_format(end_date):
            return {"error": f"Incorrect date format for 'end_date': <{end_date}>, use <YYYY-MM-DD> format"}

        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

        if start_date > end_date:
            return {"error_message": f"'start_date' must be <= 'end_date'",} 
        
        quqery_result = get_employees_born_in_period_from_department(start_date, end_date, department_id)

        if not quqery_result:
            return {"msg": f"There are no employees from {department.name} department, born in period from {start_date} to {end_date}"}

        return employees_list_schema.dump(quqery_result)

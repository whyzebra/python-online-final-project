from flask import request
from flask_restful import Resource
from rest_api.marshmallow_schemas import employee_schema, employees_list_schema
from rest_api.common import validate_datetime_format
from rest_api.models import db
from datetime import datetime

from rest_api.service import (
    get_all_employees, 
    get_employee_by_id, 
    get_employees_born_on_date, 
    get_employees_born_in_period,

    add_employe,
    update_employee,
    delete_employee
)


# /api/v1/employee/<int:employee_id>
class EmployeeResource(Resource):
    def get(self, employee_id):
        employee = get_employee_by_id(employee_id)

        if not employee:
            return {'error': f'employee with id <{employee_id}> does not exist'}

        return employee_schema.dump(employee)

    def delete(self, employee_id):
        employee_to_delete = get_employee_by_id(employee_id)

        if not employee_to_delete:
            return {'error': f'employee with id <{employee_id}> does not exist'}

        delete_employee(employee_to_delete)
        return employee_schema.dump(employee_to_delete)

    def put(self, employee_id):
        employee = get_employee_by_id(employee_id)

        if not employee:
            return {'error': f'employee with id <{employee_id}> does not exist'}

        data = request.get_json()
        errors = employee_schema.validate(data)
    
        if errors:
            return errors, 500

        update_employee(employee, data)
        return employee_schema.dump(employee)


# /api/v1/employees/
class EmployeesListResource(Resource):
    def get(self):
        employees = get_all_employees()

        if not employees:
            return {'warning': f'there are no employees in the database'}

        return employees_list_schema.dump(employees)
    
    def post(self):
        data = request.get_json()
        errors = employee_schema.validate(data)
    
        if errors:
            return errors, 500

        new_employee = add_employe(data)
        return employee_schema.dump(new_employee)


# /api/v1/employees/born_on_date/<string:birthday>
class EmployeesBornOnDateResource(Resource):
    def get(self, date_of_birth):
        if not validate_datetime_format(date_of_birth):
            return {"error": f"Incorrect date format: <{start_date}>, use <YYYY-MM-DD> format"}

        date_of_birth = datetime.strptime(date_of_birth, '%Y-%m-%d')
        employees_born_on_date = get_employees_born_on_date(date_of_birth)

        return employees_list_schema.dump(employees_born_on_date)


# /api/v1/employees/born_in_period/?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD
class EmployeesBornInPeriodResource(Resource): 
    def get(self):
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')

        if not all([start_date, end_date]):
            return {"error": "use format <?start_date=YYYY-MM-DD&end_date=YYYY-MM-DD> to get all Employees born in a specific period"}
        
        if not validate_datetime_format(start_date):
            return {"error": f"Incorrect data format for 'start_date': <{start_date}>, use <YYYY-MM-DD> format"}
        
        if not validate_datetime_format(end_date):
            return {"error": f"Incorrect data format for 'end_date': <{end_date}>, use <YYYY-MM-DD> format"}

        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

        if start_date > end_date:
            return {"error_message": f"'start_date' must be <= 'end_date'",} 

        employees_born_in_period = get_employees_born_in_period(start_date, end_date)
        return employees_list_schema.dump(employees_born_in_period)

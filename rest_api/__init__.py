from flask import Flask
from flask_restful import Api


def create_api():
    app = Flask(__name__)
    api = Api(app)

    # Config
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    # extensions
    from rest_api.marshmallow_schemas import ma
    from rest_api.models import db
    db.init_app(app)
    ma.init_app(app)


    # ==== URL or routes ====

    # Employee resources
    from rest_api.resources import(
        EmployeeResource,
        EmployeesListResource,
        EmployeesBornOnDateResource, 
        EmployeesBornInPeriodResource
    )
    api.add_resource(EmployeeResource, '/api/v1/employee/<int:employee_id>')
    api.add_resource(EmployeesListResource, '/api/v1/employees/')
    api.add_resource(EmployeesBornOnDateResource, '/api/v1/employees/born_on_date/<string:date_of_birth>')
    api.add_resource(EmployeesBornInPeriodResource, '/api/v1/employees/born_in_period/')

    # Department resources
    from rest_api.resources import (
        DepartmentResource, 
        DepartmentsListResource,
        DepartmentEmployeesBornOnDateResource, 
        DepartmentEmployeesBornInPeriodResource
    )
    api.add_resource(
        DepartmentResource, 
        '/api/v1/department/<int:department_id>', 
        '/api/v1/department/<int:department_id>/employees',
    )
    api.add_resource(DepartmentsListResource, '/api/v1/departments/')
    api.add_resource(
        DepartmentEmployeesBornOnDateResource, 
        '/api/v1/department/<int:department_id>/employees_born_on_date/<string:date_of_birth>'
    )
    api.add_resource(
        DepartmentEmployeesBornInPeriodResource, 
        '/api/v1/department/<int:department_id>/employees_born_in_period/'
    )

    return app
